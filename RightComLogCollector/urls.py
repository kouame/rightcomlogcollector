"""RightComLogCollector URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.authtoken import views

from Collector.views import HostVewSet, LogFileVewSet, LogVewSet, ApacheLogParserVewSet, apache_process_log_file, \
    sign_in

router = routers.DefaultRouter()

router.register(r'hosts', HostVewSet)
router.register(r'logFiles', LogFileVewSet)
router.register(r'logs', LogVewSet)
router.register(r'apache_viewers', ApacheLogParserVewSet)

urlpatterns = [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth/', views.obtain_auth_token),
    url(r'^', include(router.urls)),
    url(r'^apache/process/', apache_process_log_file),
    url(r'^auth/sign_in/', sign_in),
]
