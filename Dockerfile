FROM python:3
# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
COPY . /code/
RUN pip install -r /code/requirements.txt
RUN python /code/manage.py makemigrations
RUN python /code/manage.py makemigrations Collector
RUN python /code/manage.py migrate
RUN python /code/manage.py migrate Collector
RUN python /code/manage.py loaddata user.json
