#!/usr/bin/env python
# -*- coding: utf-8 -*-
from paramiko import SSHClient, AutoAddPolicy
from paramiko.ssh_exception import NoValidConnectionsError, AuthenticationException
from scp import SCPClient, SCPException


class Server(object):

    def __init__(self, hostname, username, password):
        self.hostname = hostname
        self.username = username
        self.password = password
        self.connexion = None

    def get_connection(self):
        """
        Cette méthode à pour but d'établir une connexion avec le serveur
        elle récupère les différents dont elle a besoin depuis l'initialisation de la classe
        :return: boolean
        """
        try:
            ssh = SSHClient()
            # ssh.load_system_host_keys()
            ssh.set_missing_host_key_policy(AutoAddPolicy())
            ssh.connect(self.hostname, username=self.username, password=self.password)
            self.connexion = ssh
            return True, ssh
        except (NoValidConnectionsError, AuthenticationException) as e:
            return False, e

    def get_remote_file_log(self, remote_path, local_path=''):
        """
        Cette méthode permet de récupérer un fichier de log distant avec le protocole ssh
        :param remote_path: le chemin absolu du log sur le serveur distant
        :param local_path: le chemin de sauvegarde sur le serveur web
        :return:
        """
        self.get_connection()
        try:
            scp = SCPClient(self.connexion.get_transport())
            scp.get(remote_path, local_path=local_path)
            scp.close()
        except SCPException as e:
            print("Une erreur s'est produite : %s" % e)
