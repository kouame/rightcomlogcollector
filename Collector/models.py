import os
import re

from django.db import models
from django.utils.timezone import now

from RightComLogCollector.settings import LOG_DIR
from .service import Server


class Host(models.Model):

    server = models.CharField(help_text="L'adresse du serveur", max_length=255)
    username = models.CharField(help_text="Utilisateur du système", max_length=255)
    password = models.CharField(help_text="Mot de passe serveur", max_length=255)

    def test_connection(self):
        server = Server(hostname=self.server, username=self.username, password=self.password)
        status, response = server.get_connection()
        return {
            'status': 'Success' if status else 'Failed',
            'reason': response if not status else ''
        }

    class Meta:
        ordering = ['server']
        unique_together = ('server', 'username')


class LogFile(models.Model):

    name = models.CharField(help_text="Le nom du fichier", max_length=255, unique=True)
    path = models.CharField(help_text="Le chemin absolu du fichier", max_length=255)
    application_name = models.CharField(help_text="Le nom de l'application", max_length=255)
    desc = models.TextField(help_text="Description", blank=True)
    time = models.IntegerField(help_text="Le temps de resynchro", default=5)
    host = models.ForeignKey('Host', on_delete=models.CASCADE, related_name='file_ids')

    # def save(self, *args, **kwargs):
    #     super(LogFile, self).save(*args, **kwargs)
    #     Log.objects.get_or_create(log_file_id=self.id)

    def refresh(self):
        pass

    class Meta:
        ordering = ['name']


class Log(models.Model):
    path_store = models.CharField(help_text="Le chemin absolu du fichier de log sur le serveur de traitement",
                                  blank=True, max_length=255)
    log_file = models.ForeignKey('LogFile', on_delete=models.CASCADE, related_name='log_ids')

    def save(self, *args, **kwargs):
        filename = str(now().timestamp()) + self.log_file.name + '.log'
        local_path = os.path.join(LOG_DIR, filename)
        self.path_store = local_path
        super(Log, self).save(*args, **kwargs)
        self.save_log_file()

    def save_log_file(self):
        """
        Méthode de récupération du fichier de log distant
        :return: path: str
        """

        server = Server(hostname=self.log_file.host.server, username=self.log_file.host.username,
                        password=self.log_file.host.password)
        server.get_remote_file_log(remote_path=self.log_file.path, local_path=self.path_store)


class LogFileParser(models.Model):
    class Meta:
        abstract = True

    message = models.TextField(help_text="Message", unique=True)
    date = models.CharField(help_text="Date", blank=True, max_length=255)
    log_file = models.ForeignKey('LogFile', on_delete=models.CASCADE, related_name="%(class)s_ids")

    def get_date(self, ):
        return self.date

    def get_message(self, ):
        return self.message

    @staticmethod
    def flush(log_file_id):
        pass

    def treatment(self):
        pass


class ApacheLogParser(LogFileParser):
    class Meta:
        pass

    address_ip = models.CharField(help_text="Adresse IP", blank=True, max_length=255)
    identity = models.CharField(blank=True, max_length=255)
    user = models.CharField(help_text="Utilisateur", blank=True, max_length=255)
    status = models.CharField(help_text="Statut de la requete", blank=True, max_length=255)
    event = models.CharField(help_text="Évènement", blank=True, max_length=255)
    origin = models.CharField(help_text="Origine de la requete", blank=True, max_length=255)
    data_size = models.CharField(help_text="Quantité de données", blank=True, max_length=255)
    meta = models.CharField(help_text="Metadonnées", blank=True, max_length=255)

    def save(self, *args, **kwargs):
        self.treatment()
        super(ApacheLogParser, self).save(*args, **kwargs)

    @staticmethod
    def flush(log_file_id):
        try:
            ApacheLogParser.objects.get(log_file_id=log_file_id).delete()
            return True
        except ApacheLogParser.DoesNotExist as e:
            print(e)
            return False

    def treatment(self):
        """
        Méthode de parsing du fichier de log et récupération des informations
        :return:
        """

        regex = '([(\d\.)]+) (.*?) (.*?) \[(.*?)\] "(.*?)" (\d+) (.*?) "(.*?)" "(.*?)"'
        infos = re.match(regex, self.message).groups()

        self.address_ip = infos[0]
        self.identity = infos[1]
        self.user = infos[2]
        self.date = infos[3]
        self.event = infos[4]
        self.status = infos[5]
        self.data_size = infos[6]
        self.origin = infos[7]
        self.meta = infos[8]

    @staticmethod
    def pull(log_file_id, path):
        """
        Methode permettant de processer le fichier de log et d'enregistrer ces entrées en base de données
        :param path: le chemin du fichier à traiter
        :param log_file_id:
        :return:
        """
        if os.path.isfile(path):
            with open(path, "r") as _file:
                for line in _file.readlines():
                    ApacheLogParser.objects.create(message=line, log_file_id=log_file_id)
