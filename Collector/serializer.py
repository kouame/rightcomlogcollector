#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Host, LogFile, Log, ApacheLogParser


class HostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Host
        fields = '__all__'


class LogFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = LogFile
        fields = '__all__'


class LogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Log
        fields = '__all__'


class ApacheLogParserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApacheLogParser
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    base_currency_ownership = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = User
        fields = '__all__'
