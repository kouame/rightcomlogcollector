import unittest
from django.test import TestCase
from Collector.models import Host, LogFile, ApacheLogParser

from .settings import HOST


class HostTestCase(TestCase):
    def setUp(self) -> None:
        host = Host.objects.create(server=HOST, username='ubuntu', password='QWErty12')
        self.host = host

    def test_connection(self):
        response = self.host.test_connection()
        self.assertEqual(response.get('status'), 'Success')

        self.host.password = 'fake_password'
        response = self.host.test_connection()
        self.assertEqual(response.get('status'), 'Failed')


class LogFileTestCase(TestCase):
    pass
