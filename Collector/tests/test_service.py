#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import os
from Collector.service import Server
from .settings import HOST


def remove_file(path):
    if os.path.isfile(path):
        os.remove(path)


class MyTestCase(unittest.TestCase):

    def setUp(self):
        self.server = Server(hostname=HOST, username='ubuntu', password='QWErty12')

    def test_get_connection(self):
        # pas une bonne pratique de laisser des identifiant dans du code
        # juste un petit serveur créé au passage pour le test qui sera détruit dans 96H
        status, response = self.server.get_connection()
        self.assertTrue(status)

        self.server.password = 'fake_password'
        status, response = self.server.get_connection()
        self.assertFalse(status)

    def test_remote_file_log(self):

        remote_path = '/home/ubuntu/test.log'
        self.server.get_remote_file_log(remote_path=remote_path)
        self.assertTrue(os.path.isfile('test.log'))
        remove_file('test.log')

        remote_path2 = '/home/ubuntu/test2.log'
        self.server.get_remote_file_log(remote_path=remote_path2)
        self.assertFalse(os.path.isfile('test2.log'))


if __name__ == '__main__':
    unittest.main()
