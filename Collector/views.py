from django.contrib.auth import authenticate
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin, DestroyModelMixin, CreateModelMixin, \
    UpdateModelMixin
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .models import Host, LogFile, Log, ApacheLogParser
from .serializer import HostSerializer, LogFileSerializer, LogSerializer, ApacheLogParserSerializer, UserSerializer


class HostVewSet(CreateModelMixin, UpdateModelMixin, RetrieveModelMixin, ListModelMixin, DestroyModelMixin,
                 GenericViewSet):
    """API endpoint that allows host to be viewed or edited."""

    queryset = Host.objects.all()
    serializer_class = HostSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['server', 'username']
    search_fields = ['username', 'server']
    ordering_fields = ['username', 'server']


class LogFileVewSet(CreateModelMixin, UpdateModelMixin, RetrieveModelMixin, ListModelMixin, DestroyModelMixin,
                    GenericViewSet):
    """API endpoint that allows log files to be viewed or edited."""

    queryset = LogFile.objects.all()
    serializer_class = LogFileSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['name', 'application_name', 'host']
    search_fields = ['name', 'application_name']
    ordering_fields = ['name', 'application_name', 'time']


class LogVewSet(RetrieveModelMixin, ListModelMixin, DestroyModelMixin, GenericViewSet):
    """API endpoint that allows log to be viewed or edited."""

    queryset = Log.objects.all()
    serializer_class = LogSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['log_file']


class ApacheLogParserVewSet(RetrieveModelMixin, ListModelMixin, DestroyModelMixin, GenericViewSet):
    """API endpoint that allows log to be viewed or edited."""

    queryset = ApacheLogParser.objects.all()
    serializer_class = ApacheLogParserSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['log_file', 'address_ip', 'status', 'event', 'origin']
    search_fields = ['log_file', 'address_ip', 'status', 'event', 'origin']
    ordering_fields = ['date', ]


@api_view(['POST'])
@permission_classes((AllowAny,))
def sign_in(request):
    data = request.data.copy()
    username = data['username']
    password = data['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        # A backend authenticated the credentials
        token = Token.objects.get_or_create(user=user)
        user_object = UserSerializer(user)
        return Response({"token": token[0].key, "user": user_object.data})
    else:
        # No backend authenticated the credentials
        return Response({"error": "user don't exist"})


@api_view(['POST'])
def apache_process_log_file(request):
    data = request.data.copy()
    log_file = int(data['log_file'])
    # création du log
    try:
        # todo lancer cette opération dans un thread
        log = Log.objects.create(log_file_id=log_file)
        ApacheLogParser.pull(path=log.path_store, log_file_id=log_file)
        return Response({"opération": 'Success'})

    except Exception as e:
        print(e)
        return Response({"opération": 'Failed', 'error': 'Une erreur s\'est produite'})
