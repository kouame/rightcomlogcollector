# RightComLogCollctor 
est une application qui permet de récupérer des logs d'une application sur plusieurs serveurs.
Elle est développée en Python avec le framework Django . C'est une application rest qui dispose des urls suivantes

## Download project

git clone https://kouame@bitbucket.org/kouame/rightcomlogcollector.git

## Install

    Docker-compose build
    docker-compose up -d

## User 
Pour toutes les actions de création, modification et suppression il faudra etre authentifié.
Un utilisateur par défaut a été créé avec les credentials suivants:
Login : `admin` Password: `neversaynever`

### GetToken
    POST `/auth/signin/`
**Request parameters**  
  
Name            | In   | Type    | Description  
--------------- | ---- | ------- | --------------------------  
username        | body | string  | the login.  
password           | body | string  | Password.

**Success**  
Code            | Reason     
----------------|------------------------------------------------------  
`200 - Get` | Request was successful and resources has been returned .  

**Response parameters** 
Name                      | In   | Type   | Description  
------------------------- | ---- | ------ | ------------------------------------------------------------  
token                        | body | string | user token.  


Pour les requetes de création, modification et suppression il faudra ajouter le token dans le header
    `Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b`

## Example

A titre d'exemple nous allons créé un host, un logFile et ensuite processer avec le formatter apache et verrons les résultats

### Host
    POST `/hosts/`

**Request parameters**  
  
Name            | In   | Type    | Description  
--------------- | ---- | ------- | --------------------------  
server        | body | string  | Server address.  
username        | body | string  | the login.  
password           | body | string  | Password.

**Success**  
Code            | Reason     
----------------|------------------------------------------------------  
`201 - Get` | Request was successful and resources has been created . 


### LogFile
    POST `/logFiles/`

**Request parameters**  
  
Name            | In   | Type    | Description  
--------------- | ---- | ------- | --------------------------  
name        | body | string  | Name of log file.  
path        | body | string  | remote path for log file.  
application_name           | body | string  | application name type.
desc           | body | string  | Description.
time           | body | integer  | time in minutes.
host           | body | integer  | id of host.

**Success**  
Code            | Reason     
----------------|------------------------------------------------------  
`201 - Get` | Request was successful and resources has been created . 

### Apache Process LogFile
    POST `/apache/process/`

**Request parameters**  
  
Name            | In   | Type    | Description  
--------------- | ---- | ------- | --------------------------  
log_file        | body | integer  | Id of log file.  

**Success**  
Code            | Reason     
----------------|------------------------------------------------------  
`200 - Get` | Request was successful and resources has been processed . 

**Response parameters**  
  
Name            | In   | Type    | Description  
--------------- | ---- | ------- | --------------------------  
opération        | body | string  | Status of processing.  

### Apache Log
    GET `/apache_viewers/`

**Success**  
Code            | Reason     
----------------|------------------------------------------------------  
`200 - Get` | Request was successful and resources has been returned . 

**Response parameters**  
  
Name            | In   | Type    | Description  
--------------- | ---- | ------- | --------------------------  
message        | body | string  | message.  
date        | body | string  | date.  
address_ip        | body | string  | message.  
status        | body | string  | status.  
data_size        | body | string  | data_size.  
event        | body | string  | event.  
origin        | body | string  | origin.  
meta        | body | string  | meta.  